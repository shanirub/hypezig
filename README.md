HYPEZIG is an Android app to browse local cultural events in Leipzig, Germany.

## Technical details

The app currently scrapes the events listed on the website https://kreuzer-leipzig.de which is a
popular website to list cultural events in Leipzig. All events within a time range of one month are
downloaded.

Scraping is only performed when the used explicitly downloads new contents.


## License

This software is released under the terms of the [GNU General Public License](http://www.gnu.org/licenses/gpl-3.0.html).
