package com.kolloware.hypezigapp.models.filters;

import android.util.Log;

import com.kolloware.hypezigapp.models.Event;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.kolloware.hypezigapp.BaseApplication.LOG_UI;

public class DateFilter {

    private Date dateFrom, dateTo;

    public DateFilter(Date inDateFrom, Date inDateTo) {
        this.dateFrom = getStartOfDay(inDateFrom);

        // Add one day to include all dates of the same day
        Calendar calendarDateTo = Calendar.getInstance();
        calendarDateTo.setTime(getStartOfDay(inDateTo));
        calendarDateTo.add(Calendar.DAY_OF_MONTH, 1);

        this.dateTo = calendarDateTo.getTime();
    }

    public void applyFilter(List<Event> input, List<Event> output) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".applyFilter() called with: input = [" + input
                + "], output = [" + output + "]");

        output.clear();

        for (Event forEvent : input) {

            if ((forEvent.date.compareTo(dateFrom) >= 0)
            && (forEvent.date.compareTo(dateTo) < 0)) {
                output.add(forEvent);
            }
        }
    }

    private Date getStartOfDay(Date inDate) {
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(inDate);

        // Set time to zero
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }
}
