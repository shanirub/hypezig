package com.kolloware.hypezigapp.models;

import android.util.Log;

import com.kolloware.hypezigapp.models.filters.CategoryFilter;
import com.kolloware.hypezigapp.models.filters.DateFilter;
import com.kolloware.hypezigapp.models.filters.DatePreset;
import com.kolloware.hypezigapp.db.queries.QueryStrategy;
import com.kolloware.hypezigapp.db.queries.SortByDate;
import com.kolloware.hypezigapp.models.filters.TodayPreset;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.kolloware.hypezigapp.BaseApplication.LOG_APP;

public class Model {

    private static Model instance = null;

    private ArrayList<Event> orderedEvents = new ArrayList<>();
    private ArrayList<Event> filteredEvents = new ArrayList<>();
    private ArrayList<Event> favorites = new ArrayList<>();

    private QueryStrategy queryStrategy = new SortByDate();
    private DateFilter dateFilter;
    private CategoryFilter categoryFilter = new CategoryFilter();

    private Model() {
        Log.d(LOG_APP, getClass().getSimpleName() + " constructed");

        DatePreset todayPreset = new TodayPreset();
        dateFilter = new DateFilter(todayPreset.getDateFrom(), todayPreset.getDateTo());
    }

    public static Model getInstance() {
        Log.d(LOG_APP, Model.class.getName() + ".getInstance() called");

        if (instance == null) instance = new Model();
        return instance;
    }

    public void applyFilter() {
        Log.d(LOG_APP, getClass().getSimpleName() + ".applyFilter() called");

        List<Event> localResult = new ArrayList<>();
        dateFilter.applyFilter(orderedEvents, localResult);
        categoryFilter.applyFilter(localResult, filteredEvents);
    }

    public List<Event> getOrderedEvents() {
        return orderedEvents;
    }

    public List<Event> getFilteredEvents() {
        return filteredEvents;
    }

    public QueryStrategy getQueryStrategy() {
        return queryStrategy;
    }

    public List<Event> getFavorites() { return favorites; }

    public CategoryFilter getCategoryFilter() {
        return categoryFilter;
    }

    public void setQueryStrategy(QueryStrategy queryStrategy) {
        Log.d(LOG_APP, getClass().getSimpleName() + ".setQueryStrategy() called");

        this.queryStrategy = queryStrategy;
    }

    public void updateDateFilter(Date dateFrom, Date dateTo) {
        Log.d(LOG_APP, getClass().getSimpleName() + ".updateDateFilter() called");

        this.dateFilter = new DateFilter(dateFrom, dateTo);
        applyFilter();
    }
}
